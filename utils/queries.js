const db = require('./sql_config');
const { name, version, description } = require('../package.json');
const tokenGen = require('./tokenGen');

// generate timestamp for current time
const getTimestamp = () => {
    return Math.floor(Date.now() / 1000).toString()
};


/**
 * Add a bookmark to the database.
 */
const addBookmark = (request, response) => {
    let { link, title } = request.body;
    db.query('INSERT INTO bookmarks (link,title,time_created) VALUES ($1,$2,$3)', [link, title, getTimestamp()], (error, results) => {
        if (error) {
            response.status(400).send('ERROR 400: Bad Request!')
            throw error
        }
        db.query("UPDATE stats SET stat_count = stat_count + 1 WHERE stat_name = $1", ['addBookmark'], (err, res) => {
            if (err) {
                response.status(400).send('ERROR 400: BAD Request!')
                throw err
            }
            response.status(201).send('SUCCESS 201: Bookmark added')
        })

    })

};

/**
 * Delete a bookmark from the database.
 */
const deleteBookmark = (request, response) => {
    let uuid = parseInt(request.params.id);
    db.query('DELETE FROM bookmarks WHERE uuid=$1', [uuid], (error, results) => {
        if (error) {
            response.status(406).send('ERROR 406: Bookmark not found')
            throw error
        }
        db.query("UPDATE stats SET stat_count = stat_count + 1 WHERE stat_name = $1", ['deleteBookmark'], (err, res) => {
            if (err) {
                response.status(400).send('ERROR 400: BAD Request!')
                throw err
            }
            response.status(200).send('SUCCESS 200: Bookmark deleted')
        })

    })
};

/**
 * Edit a bookmark from the database.
 */
const editBookmark = (request, response) => {
    let uuid = parseInt(request.params.id);
    let { link, title } = request.body;
    db.query('UPDATE bookmarks SET link=$1,title=$2 WHERE uuid=$3', [link, title, uuid], (error, results) => {
        if (error) {
            response.status(400).send('ERROR 400: Bad Request!')
            throw error
        }
        db.query("UPDATE stats SET stat_count = stat_count + 1 WHERE stat_name = $1", ['editBookmark'], (err, res) => {
            if (err) {
                response.status(400).send('ERROR 400: BAD Request!')
                throw err
            }
            response.status(200).send('SUCCESS 200: Bookmark updated')
        })

    })
};



/**
 * GET all the bookmarks from the database.
 */
const getBookmarks = (request, response) => {
    db.query('SELECT * FROM bookmarks ORDER BY bookmarks.uuid ASC', (error, results) => {
        if (error) {
            response.status(400).send('ERROR 400: BAD Request!')
            throw error
        }
        db.query("UPDATE stats SET stat_count = stat_count + 1 WHERE stat_name = $1", ['getBookmarks'], (err, res) => {
            if (err) {
                response.status(400).send('ERROR 400: BAD Request!')
                throw err
            }
            response.status(200).json(results.rows);
        })

    })
};

/**
 * GET all the statistics from the database.
 */
const getStats = (request, response) => {
    console.log(request.username + "login success")
    db.query('SELECT stat_name,stat_count FROM stats', (error, results) => {
        if (error) {
            response.status(400).send('ERROR 400: BAD Request!')
            throw error
        }
        db.query("UPDATE stats SET stat_count = stat_count + 1 WHERE stat_name = $1", ['getStats'], (err, res) => {
            if (err) {
                response.status(400).send('ERROR 400: BAD Request!')
                throw err
            }
            response.status(200).json({"data":results.rows});
        })

    })
}

/**
 * SEND 404 Response.
 */
const update404stat = (request, response) => {
    db.query("UPDATE stats SET stat_count = stat_count + 1 WHERE stat_name = $1", ['error404'], (err, res) => {
        if (err) {
            response.status(400).send('ERROR 400: BAD Request!')
            throw err
        }
        response.status(404).send("404: NOT FOUND!");
    })
}

/**
 * SEND Index Response.
 */
const updateIndexStat = (request, response) => {
    db.query("UPDATE stats SET stat_count = stat_count + 1 WHERE stat_name = $1", ['index'], (err, res) => {
        if (err) {
            response.status(400).send('ERROR 400: BAD Request!')
            throw err
        }
        response.status(200).json({
            name: name,
            version: version,
            description: description
        });
    })
}

/**
 * Get user auth
 */
const getAuth = (request, response) => {
    let { username, password } = request.body;

    if (username && password) {
        db.query("SELECT * FROM accounts WHERE username = $1 AND pass = $2", [username, password], (err, res) => {

            if (res.rows.length > 0) {
                return response.status(200).json(JSON.stringify({
                    "status": "authorized",
                    "token": tokenGen.generateAccessToken(username)
                }));
            } else {
                response.status(401).json(JSON.stringify({ "status": "unauthorized" }));
            }
            response.end();
        });
    } else {
        response.send('Please enter Username and Password!');
        response.end();
    }

}


module.exports = {
    getBookmarks,
    addBookmark,
    deleteBookmark,
    getStats,
    update404stat,
    updateIndexStat,
    editBookmark,
    getAuth
}
