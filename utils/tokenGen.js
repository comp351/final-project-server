const jwt = require('jsonwebtoken');

const TOKEN_SECRET = 'a9b2471bb08c12e978025294a7ef36e2f4e98f004f389cfeb8f7a4a83c826f6041f07f02c949459227dedc05aa80bb8899f801b236235713b17d4b578d18a9ca';


const generateAccessToken = (username) => {
    return jwt.sign({ "username": username }, TOKEN_SECRET, { expiresIn: 100 });
}

const authenticateToken = (request, response, next) => {
    const authHeader = request.headers['x-access-token']
    const token = authHeader && authHeader.split(' ')[1]
    console.log(authHeader)
    console.log(token)
    if (token == null) return response.sendStatus(401)

    jwt.verify(token, TOKEN_SECRET, (err, decoded) => {
        console.log(decoded.username)

        request.username = decoded.username

        next()
    })
}


module.exports = {
    generateAccessToken,
    authenticateToken
};