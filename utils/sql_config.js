const Pool = require('pg').Pool;
const fs = require('fs');
const path = require("path");

const pool = new Pool({
  user: 'doadmin',
  host: 'db-postgresql-tor1-05045-do-user-8577088-0.b.db.ondigitalocean.com',
  database: 'finalproject',
  password: 'ul5gb1lmkjmmgtlb',
  port: 25060,
  ssl: {
    rejectUnauthorized: false,
    ca: fs.readFileSync(path.resolve(__dirname, "./ca-certificate.crt")).toString()
  },
});


module.exports = pool;