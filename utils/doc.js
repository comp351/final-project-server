const pjson = require('../package.json');

const expressdocsOptions = {
  info: {
    version: pjson.version,
    title: 'CloudMark API',
    description: 'Documentation for cloudmark API',
    license: {
      name: 'MIT',
    },
  },
  servers: [
    {
      "url": "https://final-project-server-3p9ru.ondigitalocean.app",
      "description": "API server"
    }
  ],
  filesPattern: '../routes/*.js',
  swaggerUIPath: '/api/v' + pjson.version.charAt(0) + '/docs',
  exposeSwaggerUI: true,
  exposeApiDocs: true,
  apiDocsPath: '/api/v' + pjson.version.charAt(0) + '/api-docs',
  baseDir: __dirname,
};

module.exports = expressdocsOptions;
