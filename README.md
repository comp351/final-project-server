# A Simple bookmark CRUD API built using NodeJS.

Documentation: https://final-project-server-3p9ru.ondigitalocean.app/api/v1/docs/

### Prerequisites

* NodeJS
* npm

## Deployment

Deploy to your server to access the API. 

## Built With

* NodeJS
* ExpressJS
* Postgresql

## Usage

### Bookmark Endpoints

* Create a Bookmark -POST- /api/v_/bookmarks/create

```json
{
	"link":"https://www.google.com",
	"title":"Google"
}
```
* Delete a bookmark -DELETE- /api/v_/bookmarks/:id

* Display bookmarks -GET- /api/v_/bookmarks

* Update a bookmark -PUT- /api/v_/bookmarks/edit/:id

## Authors

* **Chinmay Sharma**

## License

This project is licensed under the MIT License.
