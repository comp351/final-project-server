/**
 * A Bookmark
 * @typedef {object} Bookmark
 * @property {string} link.required - Link
 * @property {string} title.required - The title
 */
const express = require('express');
const router = express.Router();
const queries = require('../utils/queries.js');
const cors = require('cors');

//CORS
// const whitelist = ['https://final-project-client-rnwju.ondigitalocean.app','https://final-project-server-3p9ru.ondigitalocean.app', 'https://ondigitalocean.app']
// const corsOptions = {
//   origin: (origin, callback) => {
//     if (whitelist.indexOf(origin) !== -1) {
//       callback(null, true)
//     } else {
//       callback(new Error('Not allowed by CORS'))
//     }
//   },
// }

// router.use(cors(corsOptions));

router.use(cors());


// Bookmark routes

/**
 * POST /api/v1/bookmarks/create
 * @summary Add bookmarks to the database.
 * @param {Bookmark} request.body.required - bookmark info
 * @return {string} 201 - Bookmark Created
 * @return {string} 400 - Bad Request
 */
router.post('/create', queries.addBookmark);

/**
 * DELETE /api/v1/bookmarks/{id}
 * @summary Delete bookmarks from the database.
 * @param {number} id.path.required - Bookmark id
 * @return {string} 200 - Bookmark Deleted
 * @return {string} 406 - Bookmark not found
 */
router.delete('/:id', queries.deleteBookmark);

/**
 * PUT /api/v1/bookmarks/edit/{id}
 * @summary Edit a bookmark from the database.
 * @param {number} id.path.required - Bookmark id
 * @param {Bookmark} request.body.required - bookmark info
 * @return {string} 200 - Bookmark Updated
 * @return {string} 400 - Bad Request
 */
router.put('/edit/:id', queries.editBookmark);

/**
 * GET /api/v1/bookmarks
 * @summary Get all the bookmarks
 * @return {string} 200 - success response
 * @return {string} 400 - Bad Request
 */
router.get('/', queries.getBookmarks);


module.exports = router;