/**
 * A User
 * @typedef {object} User
 * @property {string} username.required - Username
 * @property {string} password.required - Password
 */

const express = require('express');
const router = express.Router();
const queries = require('../utils/queries.js');
const tokenGen = require('../utils/tokenGen');
const cors = require('cors');

//CORS
// const whitelist = ['https://final-project-client-rnwju.ondigitalocean.app','https://final-project-server-3p9ru.ondigitalocean.app', 'https://ondigitalocean.app']
// const corsOptions = {
//   origin: (origin, callback) => {
//     if (whitelist.indexOf(origin) !== -1) {
//       callback(null, true)
//     } else {
//       callback(new Error('Not allowed by CORS'))
//     }
//   },
// }

// router.use(cors(corsOptions));

router.use(cors());

//Index Page

/**
 * GET /api/v1
 * @summary Get basic information about the API
 * @return {string} 200 - success response
 */
router.get('/', queries.updateIndexStat);

/**
 * GET /api/v1/stats
 * @summary Get all the statistics
 * @return {string} 200 - success response
 */
router.get('/stats', tokenGen.authenticateToken, queries.getStats);

/**
 * POST /api/v1/auth
 * @summary user auth
 * @param {User} request.body.required - user info
 * @return {string} 200 - success response
 */
router.post('/auth', queries.getAuth);

module.exports = router;