const express = require('express')
const app = express()
const port = process.env.PORT || 3000
const cookieParser = require('cookie-parser');
const indexRouter = require('./routes/indexRouter');
const bookmarkRouter = require('./routes/bookmarkRouter');
const pjson = require('./package.json');
const expressJSDocSwagger = require('express-jsdoc-swagger');
const options = require('./utils/doc');
const queries = require('./utils/queries.js');

// app.use(cors())


const url_prepend = "/api/v" + pjson.version.charAt(0);
console.log(url_prepend);

expressJSDocSwagger(app)(options);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


app.use(url_prepend + '/', indexRouter);

app.use(url_prepend + '/bookmarks', bookmarkRouter);

/**
 * ANY unauthorized
 * @summary 404 Error Handling
 * @return {object} 404 - success response
 */
app.get('/*', queries.update404stat);


// Run Server
app.listen(port, () => {
  console.log(`App running on port ${port}.`)
});
