create table bookmarks (
    uuid BIGSERIAL 	PRIMARY KEY 	NOT NULL,
    link varchar 	UNIQUE 		NOT NULL,
    title varchar 	NOT NULL,
    time_created varchar
);

create table accounts (
    admin_id SERIAL PRIMARY KEY,
    username varchar UNIQUE NOT NULL,
    pass varchar NOT NULL
);


create table stats (
    id SERIAL   PRIMARY KEY,
    stat_name varchar  UNIQUE   NOT NULL,
    stat_count INT   DEFAULT 0
);

INSERT INTO stats (stat_name,stat_count) VALUES ('getBookmarks',0);
INSERT INTO stats (stat_name,stat_count) VALUES ('deleteBookmark',0);
INSERT INTO stats (stat_name,stat_count) VALUES ('editBookmark',0);
INSERT INTO stats (stat_name,stat_count) VALUES ('addBookmark',0);
INSERT INTO stats (stat_name,stat_count) VALUES ('getStats',0);
INSERT INTO stats (stat_name,stat_count) VALUES ('index',0);
INSERT INTO stats (stat_name,stat_count) VALUES ('error404',0);


INSERT INTO accounts (username,pass) VALUES ('admin','admin');
